<?php


namespace VendingMachine;


class Constants
{
    const ACCEPTED_COINS = [
        "5c" => 0.05, "10c" => 0.1, "20c" => 0.2, "50c" => 0.5, "$1" => 1.0, "$2" => 2.0
    ];
    const CUSTOMER_COINS = [
        "$2" => [], "$1" => [], "50c" => [], "20c" => [], "10c" => [], "5c" => []
    ];
    const AVAILABLE_CHANGE = [2, 4, 2, 3, 4, 1];
    const AVAILABLE_CHANGE_SIGN = ["$2", "$1", "50c", "20c", "10c", "5c"];
    const ITEMS = [
        "Snickers" => 3, "Bounty" => 0, "Mars" => 1, "Twix" => 2, "Wispa" => 2,
        "Twirl" => 2, "Yorkie" => 3, "Aero" => 0, "Double Decker" => 3, "Galaxy" => 2,
        "Crunchie" => 3, "Picnic" => 2, "Kit Kat" => 2, "Lion Bar" => 3, "Oreo" => 2,
        "Toffee Crisp" => 1, "Boost" => 1
    ];
    const SLOT = [
        "Slot 1" => 1.05, "Slot 2" => 1.00, "Slot 3" => 1.25, "Slot 4" => 2.00, "Slot 5" => 1.80, "Slot 6" => 0.75,
        "Slot 7" => 1.80, "Slot 8" => 1.80, "Slot 9" => 0.75, "Slot 10" => 1.80, "Slot 11" => 1.80, "Slot 12" => 1.25,
        "Slot 13" => 2.00, "Slot 14" => 1.80, "Slot 15" => 2.00, "Slot 16" => 2.00, "Slot 17" => 1.50
    ];
    const AVAILABLE_CHANGE_IN_COINS = [200, 100, 50, 20, 10, 5];
}
