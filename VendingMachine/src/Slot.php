<?php


namespace VendingMachine;

use Exception;
use VendingMachine\Product;

class Slot
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var float
     */
    private $price;
    /**
     * @var Product
     */
    private $product;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return (float)number_format($this->price, 2);
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * Slot constructor.
     * @param string $id
     * @param float $price
     * @param Product $product
     */
    public function __construct(string $id, float $price, Product $product)
    {
        $this->id = $id;
        $this->price = $price;
        $this->product = $product;
    }

    /**
     * @param string $slotId
     * @return bool
     * @throws Exception
     */
    public function isSlotAvailable(string $slotId): bool
    {
        if ($slotId == strtolower($this->getId()) && $this->getProduct()->getQuantity() <= 0) {
                $ex = "Slot is empty!" . PHP_EOL;
                $ex .= '-----------------' . PHP_EOL . PHP_EOL;
                throw new Exception($ex);
        }
        return true;
    }
}
