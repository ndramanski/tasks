<?php


namespace VendingMachine;


use Exception;
use VendingMachine\Product;
use VendingMachine\Slot;

class VendingMachine
{
    /**
     * @var array
     */
    private $slots = [];
    /**
     * @var array
     */
    private $changeBox = [];

    /**
     * @var int
     */
    private $entered = 0;

    /**
     * @return array
     */
    public function getChangeBox(): array
    {
        return $this->changeBox;
    }

    /**
     * @param array $changeBox
     */
    public function setChangeBox(array $changeBox): void
    {
        $this->changeBox = $changeBox;
    }

    /**
     * VendingMachine constructor.
     * @param array $slots
     * @param array $changeBox
     */
    public function __construct(array $slots, array $changeBox)
    {
        $this->slots = $slots;
        $this->changeBox = $changeBox;
    }
}
