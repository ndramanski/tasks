<?php


namespace VendingMachine;


interface CalculationsInterface
{
    /**
     * @param int $deposit
     * @param Slot $slot
     * @return float
     */
    public function calculateChange(int $deposit, Slot $slot): float;

    /**
     * @param array $changeBox
     * @return bool
     */
    public function availableChange(array $changeBox): bool;

    /**
     * @param array $coins
     * @return float
     */
    public function calculateEnteredCoins(array $coins): float;

}