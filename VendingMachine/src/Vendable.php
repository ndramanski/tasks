<?php

namespace VendingMachine;

interface Vendable
{

    /**
     * @param float $deposit
     * @param Slot $slot
     * @return bool
     */
    public function isDepositEnough(float $deposit, Slot $slot): bool;

    /**
     * @param VMConfiguration $slot
     * @param string $input
     * @return string
     */
    public function slotManipulation(VMConfiguration $slot, string $input): string;
}
