<?php

namespace VendingMachine;


class VMService
{
    public function userInput()
    {
        echo 'Enter = ';
    }

    /**
     * @param VMConfiguration $vMachine
     */
    public function printMachineInfo(VMConfiguration $vMachine)
    {
        $info = "Welcome to the Vending Machine simulator!" . PHP_EOL . PHP_EOL;
        $info .= "The vending machine contains the following products" . PHP_EOL . PHP_EOL;
        /** @var Slot $slot */
        foreach ($vMachine->getSlots() as $slot) {
            $info .= $slot->getId() . " - " . $slot->getProduct()->getQuantity() . " x "
                . $slot->getProduct()->getName() . " = " . $slot->getPrice() . PHP_EOL;
        }
        $info .= PHP_EOL . "The vending machine accepts the following coins" . PHP_EOL
            . "5c 10c 20c 50c $1 $2" . PHP_EOL . PHP_EOL
            . "Please insert coins one at a time and pressing enter after each, e.g. $2 or 5c" . PHP_EOL . PHP_EOL
            . "To vend from a slot type slot command, e.g. slot 1" . PHP_EOL . PHP_EOL;
        echo $info;
    }

    /**
     * @param Calculations $calculate
     */
    public function viewEnteredAmount(Calculations $calculate)
    {
        echo PHP_EOL . 'Tendered : ' . $calculate->getEntered() . PHP_EOL . PHP_EOL;
    }

    /**
     * @param $itemName
     * @param $changeCoins
     */
    public function printVendInfo($itemName, $changeCoins)
    {
        $vendInfo = PHP_EOL . 'Enjoy !' . PHP_EOL;
        $vendInfo .= PHP_EOL . 'Item = ' . $itemName;
        if (array_sum($changeCoins) > 0) {
            $vendInfo .= PHP_EOL . 'Change = ' . implode(",", $changeCoins) . PHP_EOL;
        } else {
            $vendInfo .= PHP_EOL . 'Change = ' . '0' . PHP_EOL;
        }
        $vendInfo .= '-----------------' . PHP_EOL . PHP_EOL;
        echo $vendInfo;
    }

    public function calculateChange(Calculations $calculate, VMConfiguration $slot, $input)
    {
        $result = 0.00;
        /** @var Slot $singleSlot */
        foreach ($slot->getSlots() as $singleSlot) {
            if (strtolower($singleSlot->getId()) == $input) {
                $result = $calculate->getEntered() - $singleSlot->getPrice();
            }
        }
        return (float)$result;
    }
}
