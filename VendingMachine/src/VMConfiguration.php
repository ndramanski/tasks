<?php


namespace VendingMachine;

class VMConfiguration
{

    private $slot;
    private $product;
    private $slots = [];
    private $products = [];

    /**
     * @return array
     */
    public function getSlots(): array
    {
        return $this->slots;
    }

    /**
     * VMConfiguration constructor.
     * @param array $slots
     * @param array $products
     */
    public function __construct()
    {
        $this->setSlots();
    }

    /**
     * @return Slot
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * @param Slot $slot
     */
    public function setSlot($slot): void
    {
        $this->slot = $slot;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }

    public function setSlots()
    {
        foreach (Constants::ITEMS as $name => $quantity) {
            $this->setProduct(new Product($name, $quantity));
            $this->products[] = $this->getProduct();
        }
        foreach (Constants::SLOT as $id => $price) {
            $key = explode(' ', $id);
            $this->setSlot(new Slot($id, $price, $this->products[$key[1] - 1]));
            $this->slots[] = $this->getSlot();
        }
    }
}
