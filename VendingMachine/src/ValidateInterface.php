<?php

namespace VendingMachine;

interface ValidateInterface
{
    /**
     * @param int $deposit
     * @return bool
     */
    public function validateCoins(int $deposit):bool;

    /**
     * @param string $input
     */
    public function validateInput(string $input);
}
