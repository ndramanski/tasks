<?php

namespace VendingMachine;

use Exception;

class VMController
{
    /**
     * @param VendingMachine $machine
     */
    private $machine;
    /**
     * @param Calculations $calculation
     */
    private $calculation;
    /**
     * @param VMService $service
     */
    private $service;
    /**
     * @param VMConfiguration $configuration
     */
    private $configuration;

    /**
     * @param VendingMachine $machine
     */
    public function setMachine($machine): void
    {
        $this->machine = $machine;
    }

    /**
     * VMController constructor.
     * @param Calculations $calculation
     * @param VMService $service
     * @param VMConfiguration $configuration
     */
    public function __construct(Calculations $calculation, VMService $service, VMConfiguration $configuration)
    {
        $this->calculation = $calculation;
        $this->service = $service;
        $this->configuration = $configuration;
        $this->setMachine(new VendingMachine($this->configuration->getSlots(), Constants::AVAILABLE_CHANGE));
    }

    public function vend()
    {
        $renderWelcomeOutput = true;
        while (true) {
            if ($renderWelcomeOutput === true) {
                $renderWelcomeOutput = false;
                $this->service->printMachineInfo($this->configuration);
            }
            $this->service->userInput();
            $input = strtolower(trim(fgets(STDIN)));
            $validator = $this->calculation->validateInput($input);
            switch ($validator) {
                case 'slot':
                    try {
                        $productName = $this->calculation->slotManipulation($this->configuration, $input);
                        $change = $this->service->calculateChange($this->calculation, $this->configuration, $input);
                        $returnedAmount = $this->calculation->handleCoins($this->machine, $change);
                        $this->calculation->clearCustomerCoins();
                        $this->service->printVendInfo($productName, $returnedAmount);
                    } catch (Exception $e) {
                        echo $e->getMessage() . PHP_EOL;
                    }
                    $this->service->printMachineInfo($this->configuration);
                    break;
                case 'coin':
                    try {
                        $this->handleCoin($input, $this->calculation);
                        $this->service->viewEnteredAmount($this->calculation);
                    } catch (Exception $e) {
                        echo $e->getMessage() . PHP_EOL;
                    }
                    break;
            }
        }
    }

    /**
     * @param string
     * @param Calculations $calculation
     * @throws Exception
     */
    public function handleCoin(string $input, Calculations $calculation): void
    {
        $calculation->addUserCoin($input);
    }
}
