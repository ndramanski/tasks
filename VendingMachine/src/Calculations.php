<?php


namespace VendingMachine;


use Exception;

class Calculations implements CalculationsInterface, ValidateInterface, Vendable
{
    private $entered = 0.00;
    private $customerCoins = Constants::CUSTOMER_COINS;

    /**
     * @param int $deposit
     * @param Slot $slot
     * @return float
     * @throws Exception
     */
    public function calculateChange(int $deposit, Slot $slot): float
    {
        $sumCoins = 0.00;
        if (!key_exists($this->validateCoins($deposit), Constants::AVAILABLE_CHANGE) ||
            !$this->availableChange(Constants::AVAILABLE_CHANGE)) {
            throw new Exception("Not enough funds or valid insertion!");
        }
        $sumCoins += floatval($deposit);
        $change = $sumCoins - $slot->getPrice();
        return $change;
    }

    /**
     * @param int $deposit
     * @return bool
     * @throws Exception
     */
    public function validateCoins(int $deposit): bool
    {
        if (!key_exists($deposit, Constants::ACCEPTED_COINS)) {
            throw new Exception("Not allowed insertion!");
        }
        return key_exists($deposit, Constants::ACCEPTED_COINS);
    }

    /**
     * @param array $changeBox
     * @return bool
     * @throws Exception
     */
    public function availableChange(array $changeBox): bool
    {
        if (array_sum($changeBox) < $this->getEntered()) {
            throw new Exception("Not enough change");
        }
        return (array_sum($changeBox) >= $this->getEntered());
    }

    /**
     * @param string $input
     ** @return string
     */
    public function validateInput(string $input): string
    {
        if (strpos($input, strtolower('slot')) !== false) {
            $result = 'slot';
        } else {
            $result = 'coin';
        }
        return $result;
    }

    /**
     * @param string
     * @throws Exception
     */
    public function addUserCoin(string $input): void
    {
        if (!key_exists($input, Constants::ACCEPTED_COINS)) {
            $ex = "No allowed coin!" . PHP_EOL;
            $ex .= '-----------------' . PHP_EOL . PHP_EOL;
            throw new Exception($ex);
        }
        if (!key_exists($input, $this->customerCoins)) {
            $this->customerCoins[$input] = (float)Constants::ACCEPTED_COINS[$input];
            $this->entered = $this->calculateEnteredCoins($this->customerCoins);
        } else {
            $this->customerCoins[$input][] = (float)Constants::ACCEPTED_COINS[$input];
            $this->entered = $this->calculateEnteredCoins($this->customerCoins);
        }
    }

    /**
     * @param array $coins
     * @return float
     */
    public function calculateEnteredCoins(array $coins): float
    {
        $total = 0;
        foreach ($coins as $coin) {
            if (is_array($coin)) {
                $total += array_sum($coin);
            } else {
                $total += $coin;
            }
        }
        return (float)number_format($total, 2);
    }

    public function getEntered()
    {
        return number_format($this->entered, 2);
    }

    public function clearCustomerCoins(): void
    {
        $this->customerCoins = Constants::CUSTOMER_COINS;
    }

    /**
     * @param VMConfiguration $slot
     * @param string $input
     * @return string
     * @throws Exception
     */
    public function slotManipulation(VMConfiguration $slot, string $input): string
    {
        /** @var Slot $singleSlot */
        foreach ($slot->getSlots() as $singleSlot) {
            if ($input == strtolower($singleSlot->getId()) && $singleSlot->isSlotAvailable($input)) {
                if ($this->isDepositEnough($this->getEntered(), $singleSlot)) {
                    $result = $singleSlot->getProduct()->getQuantity() - 1;
                    $singleSlot->getProduct()->setQuantity($result);
                    return $singleSlot->getProduct()->getName();
                } else {
                    $ex = "Entered amount is not enough!" . PHP_EOL;
                    $ex .= '-----------------' . PHP_EOL . PHP_EOL;
                    throw new Exception($ex);
                }
            }
        }
    }

    /**
     * @param VendingMachine $machine
     * @param float $change
     * @return array
     */
    public function handleCoins(VendingMachine $machine, float $change): array
    {
        $machine->setChangeBox(Constants::AVAILABLE_CHANGE);
        $amount = round($change * 100);
        $coinPattern = Constants::AVAILABLE_CHANGE_IN_COINS;
        $coinSign = Constants::AVAILABLE_CHANGE_SIGN;
        $machineMoney = $machine->getChangeBox();
        $total = [];
        $sum = 0;
        $i = 0;
        $coins = $coinPattern[0];
        $sign = $coinSign[0];
        while ($sum != $amount) {
            $sum += $coins;
            if ($sum > $amount || $machineMoney[$i]==0) {
                $sum -= $coins;
                $i++;
                $coins = $coinPattern[$i];
                $sign = $coinSign[$i];
                continue;
            }
            $total[] = $sign;
            $key = array_search($coins, $coinPattern);
            $result = $machineMoney[$key] - 1;
            array_splice($machineMoney, $key, 1, $result);
            $allCoins = array_merge($machineMoney, $this->customerCoins);
            $machine->setChangeBox($allCoins);
        }
        return $total;
    }

    /**
     * @param float $deposit
     * @param Slot $slot
     * @return bool
     * @throws Exception
     */
    public function isDepositEnough(float $deposit, Slot $slot): bool
    {
        if ($deposit < $slot->getPrice()) {
            $ex = "Not enough deposit to cover the slot price!" . PHP_EOL;
            $ex .= '-----------------' . PHP_EOL . PHP_EOL;
            throw new Exception($ex);
        }
        return ($deposit >= $slot->getPrice());
    }
}
