<?php

use VendingMachine\Slot;
use VendingMachine\Product;
use VendingMachine\VendingMachine;
use VendingMachine\Calculations;
use VendingMachine\Constants;
use VendingMachine\VMConfiguration;
use VendingMachine\VMController;
use VendingMachine\VMService;

require __DIR__ . '/vendor/autoload.php';
$calculations = new Calculations();
$service = new VMService();
$configuration = new VMConfiguration();
$controller = new VMController($calculations, $service, $configuration);
$controller->vend();
