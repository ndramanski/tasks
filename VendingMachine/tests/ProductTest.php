<?php

use PHPUnit\Framework\TestCase;
use VendingMachine\Product;

class ProductTest extends TestCase
{
    private $product;

    public function setUp(): void
    {
        $this->product = new Product("TestProduct", 3);
    }
    public function testToGetQuantityOfProduct()
    {
        $this->product->getQuantity();
        $this->assertEquals($this->product->getQuantity(), 3);
    }
    public function testToGetNameOfProduct()
    {
        $this->product->getName();
        $this->assertEquals($this->product->getName(), "TestProduct");
    }
    public function testToSetQuantityOfProduct()
    {
        $this->product->setQuantity(5);
        $this->assertEquals($this->product->getQuantity(), 5);
    }
}
