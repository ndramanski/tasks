<?php
use PHPUnit\Framework\TestCase;
use VendingMachine\Product;
use VendingMachine\Slot;

class SlotTest extends TestCase
{
    private $product;
    private $slot;

    public function setUp(): void
    {
        $this->product = new Product("TestProduct", 3);
        $this->slot = new Slot("Slot 1", 2.00, $this->product);
    }
    public function testToGetIdOfSlot()
    {
        $this->slot->getId();
        $this->assertEquals($this->slot->getId(), "Slot 1");
    }
    public function testToGetPriceOfSlot()
    {
        $this->slot->getPrice();
        $this->assertEquals($this->slot->getPrice(), 2);
    }
    public function testToGetProductInSlot()
    {
        $this->slot->getProduct();
        $this->assertEquals($this->slot->getProduct(), $this->product);
    }
    public function testToToCheckIsSlotAvailable()
    {
        $actual = $this->slot->isSlotAvailable($this->slot->getId());
        $this->assertEquals(true, $actual);
    }
}
