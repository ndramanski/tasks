<?php

use PHPUnit\Framework\TestCase;
use VendingMachine\VendingMachine;

class VendingMachineTest extends TestCase
{
    private $machine;
    const TEST_SLOTS = ["slot1","slot2","slot3"];
    const TEST_CHANGE_BOX = [1, 2, 3];

    public function setUp(): void
    {
        $this->machine = new VendingMachine(self::TEST_SLOTS, self::TEST_CHANGE_BOX);
    }
    public function testToGetChangeBoxOFMachine()
    {
        $this->machine->getChangeBox();
        $this->assertEquals($this->machine->getChangeBox(), self::TEST_CHANGE_BOX);
    }
    public function testToSetChangeBoxOFMachine()
    {
        $this->machine->setChangeBox([1, 5, 3]);
        $this->assertEquals($this->machine->getChangeBox(), [1, 5, 3]);
    }
}
