<?php

namespace newTask;

class Str
{
    /**
     * @var string
     */
    private $text;
    private $splitedText;

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * Str constructor.
     * @param string $text
     */
    public function __construct(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return int
     */
    public function len(): int
    {
        return strlen($this->getText());
    }

    /**
     * @param int $number1
     * @param int $number2
     * @return string
     */
    public function sub(int $number1, int $number2 = null): string
    {
        $result = '';
        if ($number2) {
            $result = substr($this->getText(), $number1, $number2);
        } elseif ($number1) {
            $number2 = abs($number1);
            $result = substr($this->getText(), $number1, $number2);
        }
        return $result;
    }

    /**
     * @param string $delimiter
     * @return Str
     */
    public function split(string $delimiter): Str
    {
        $this->splitedText = explode($delimiter, $this->getText());
        return $this;
    }

    /**
     * @param string $glue
     */
    public function join(string $glue): void
    {
        $this->setText(implode($glue, $this->splitedText));
    }
}
