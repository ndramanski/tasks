<?php

use newTask\Str;

require __DIR__ . '/vendor/autoload.php';

$s = new Str('The quick brown fox');
echo "{$s->len()}\n";
echo "{$s->sub(0, 3)}\n";
echo "{$s->sub(-3)}\n";
$s->split(' ')->join('_');
echo "$s\n";
