<?php

use newTask\Str;
use PHPUnit\Framework\TestCase;

class StrTest extends TestCase
{
    private $text;

    public function setUp(): void
    {
        $this->text = new Str('The quick brown fox');
    }

    public function testToGetTheText()
    {
        $this->text->getText();
        $this->assertEquals($this->text->getText(), 'The quick brown fox');
    }

    public function testToSetTheText()
    {
        $this->text->setText('The quick brown fox');
        $this->text->getText();
        $this->assertEquals($this->text->getText(), 'The quick brown fox');
    }

    public function testTheLengthOfText()
    {
        $resultValue = strlen($this->text->getText());
        $this->assertEquals($resultValue, 19);
    }

    public function testToReturnPartOfTextWithGivenTwoParameters()
    {
        $number1 = 0;
        $number2 = 3;
        $this->text->getText();
        $this->assertEquals(substr($this->text->getText(), $number1, $number2), 'The');
    }

    public function testToReturnPartOfTextWithGivenTOneParameters()
    {
        $number1 = -3;
        $this->text->getText();
        $this->assertEquals(substr($this->text->getText(), $number1, abs($number1)), 'fox');
    }

    public function testToSplitTextToParts()
    {
        $splitedText = explode(' ', $this->text->getText());
        $this->assertIsArray($splitedText);
    }

    public function testToGlueArrayToText()
    {
        $splitedText = explode(' ', $this->text->getText());
        $resultText = implode('_', $splitedText);
        $this->assertEquals($resultText, 'The_quick_brown_fox');
    }
}
