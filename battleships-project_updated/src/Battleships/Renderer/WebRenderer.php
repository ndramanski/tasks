<?php

namespace Battleships\Renderer;

use Battleships\Data\ResultDTO;
use Battleships\Helper\GameProcessor;
use Battleships\Model\Board;
use Battleships\Model\BoardField;
use Battleships\Model\State;
use Config\Elements;


class WebRenderer extends AbstractRenderer implements RendererInterface
{
    /**
     * @param string $result
     * @param State $state
     * @param bool $activateCheat
     */
    public function render($result, State $state, bool $activateCheat)
    {
        if ($result === Elements::HANDLE_GAME_OVER) {
            return $this->renderGameOver($state);
        }
        echo '<style>';
        include 'StyleSheet.css';
        echo '</style>';
        $board = $state->getBoard();
        echo "<figure>";
        echo "<table>";
        echo "<thead>";
        echo "<tr>";
        echo " <td id='game'>";
        echo "<figure>";
        echo "<img id=logo src='src/Battleships/Renderer/Resources/battleship.gif'/>";
        echo "</figure>";
        echo " </td>";
        echo " <td id='game'>";
        echo "<h2>";
        echo "<img id=symb src='src/Battleships/Renderer/Resources/target.png'/>" .
            "Battleships Game Board" .
            "<img id=symb src='src/Battleships/Renderer/Resources/target.png'/>";
        echo "</h2>";
        echo " </td>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        echo "<tr>";
        echo " <td id='game'>";
        echo "<figure>";
        echo "<pre>";
        echo str_repeat($this->newLine, 2);

        for ($colNumber = 1; $colNumber <= $board->getBoardWidth(); $colNumber++) {
            if ($colNumber === 1) {
                echo str_repeat($this->emptySpace, 2);
            }
            echo $colNumber . str_repeat($this->emptySpace, 2);
        }
        echo $this->newLine;
        foreach ($board->getGrid() as $gridColumn => $gridRow) {
            echo $board->getBoardLetterByColumnNumber($gridColumn);
            echo $this->emptySpace;
            foreach ($gridRow as $boardField) {
                $this->renderBoardField($boardField, $activateCheat);
            }
            echo $this->newLine;
        }
        echo $this->newLine;
        echo "</pre>";
        echo "</figure>";
        echo " </td>";
        echo " <td id='game'>";

        if ($result !== '') {
            switch ($result) {
                case Elements::HANDLE_RESULT_SUNK:
                    echo " <p>";
                    echo $result . PHP_EOL;
                    echo " </p>";
                    echo "<img src='src/Battleships/Renderer/Resources/ship.gif'/>";
                    break;
                case Elements::HANDLE_RESULT_HIT:
                    echo " <p>";
                    echo $result . PHP_EOL . PHP_EOL;
                    echo " </p>";
                    echo "<img src='src/Battleships/Renderer/Resources/hit.gif'/>";
                    break;
                case Elements::HANDLE_RESULT_MISS:
                    echo " <p>";
                    echo $result . PHP_EOL;
                    echo " </p>";
                    break;
                case Elements::HANDLE_RESULT_ERROR:
                    echo " <p>";
                    echo $result . PHP_EOL;
                    echo " </p>";
                    break;
            }
        }
        echo " </td>";
        echo "</tr>";
        echo "<tr>";
        echo " <td id='game'>";
        echo '
			<form name="input" id="gameForm" action="index.php" method="post">
				Enter coordinates (row, col), e.g. A5 <input type="input" size="5" name="coord" autocomplete="off" autofocus="">
				<input type="submit" id="sub" value="FIRE">
			</form>
		';
        echo " </td>";
        echo "</tr>";
        echo "</tbody>";
        echo "</table>";
        echo "</figure>";
    }

    /**
     * @param State $state
     */
    private function renderGameOver(State $state)
    {
        echo '<style>';
        include 'StyleSheet.css';
        echo '</style>';
        $result = sprintf("<p>Well done! You completed the game in %d shots.</p>", $state->getShotsTaken());
        echo $result;
        echo '<p><a href="restart.php">Play again?</a></p>';
        echo '<h3>Save results below!</h3>';
        $shots = sprintf("%d", $state->getShotsTaken());
        echo '<p>';
        echo ' <form name="input" action="save.php" method="post">';
        echo '<p>Enter your name: <input type="input" size="25" name="username" autocomplete="off" autofocus=""></p>';
        echo "<p>Your result: <input type='input' size='3' name='result' value= $shots></p>";
        echo '<p><input type="submit" id="saveRes" value="Save Result"></p>';
        echo '</p>';
        echo '</form>';
    }

    public function viewStatistics($formData)
    {
        echo '<style>';
        include 'StyleSheet.css';
        echo '</style>';
        echo "<h3>";
        echo "<img id=symb src='src/Battleships/Renderer/Resources/target.png'/>" .
            "Battleships Game Results" .
            "<img id=symb src='src/Battleships/Renderer/Resources/target.png'/>";
        echo "</h3>";
        echo '<p><a href="restart.php">Play again?</a></p>';
        echo "<figure>";
        echo "<table id='viewStat'>";
        echo "<thead id='res'>";
        echo "<tr>";
        echo " <td id='çolNo'>";
        echo "<p>No.</p>";
        echo " </td>";
        echo " <td id='çolName'>";
        echo "<p>Name</p>";
        echo " </td>";
        echo " <td id='çolRes'>";
        echo "<p>Result</p>";
        echo " </td>";
        echo " <td id='çolDate'>";
        echo "<p>Date</p>";
        echo " </td>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        /** @var ResultDTO $data */
        foreach ($formData as $data) {
            echo "<tr>";
            echo " <td id='çolNo'>";
            echo $data->getId();
            echo " </td>";
            echo " <td id='çolName'>";
            echo $data->getUsername();
            echo " </td>";
            echo " <td id='çolRes'>";
            echo $data->getResult();
            echo " </td>";
            echo " <td id='çolDate'>";
            echo $data->getCreatedOn();
            echo " </td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "</figure>";
    }
}
