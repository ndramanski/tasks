<?php

namespace Battleships\Repository;

use Battleships\Data\ResultDTO;
use Core\DataBinderInterface;
use Database\DatabaseInterface;

class ResultRepository extends DatabaseAbstract implements ResultRepositoryInterface
{

    public function __construct(DatabaseInterface $database, DataBinderInterface $dataBinder)
    {
        parent::__construct($database, $dataBinder);
    }

    public function insert(ResultDTO $resultDTO): bool
    {
        $this->db->query(
            "INSERT INTO results(username, result, created_on)
                  VALUES(?,?,?)
             "
        )->execute([
            $resultDTO->getUsername(),
            $resultDTO->getResult(),
            $resultDTO->getCreatedOn()
        ]);
        return true;
    }

    /**
     * @return \Generator|ResultDTO[]
     */
    public function findAll(): \Generator
    {
        return $this->db->query(
            "
                  SELECT id, 
                      username, 
                      result, 
                      created_on as createdOn
                  FROM results
                  ORDER BY id DESC, result ASC, created_on DESC 
            "
        )->execute()
            ->fetch(ResultDTO::class);
    }
}
