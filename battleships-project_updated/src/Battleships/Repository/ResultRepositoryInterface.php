<?php

namespace Battleships\Repository;

use Battleships\Data\ResultDTO;

interface ResultRepositoryInterface
{
    public function insert(ResultDTO $resultDTO): bool;

    /**
     * @return \Generator|ResultDTO[]
     */
    public function findAll(): \Generator;
}
