<?php

namespace Battleships\Validator;

use Battleships\Model\State;

interface ValidatorInterface
{
    /**
     * @param mixed $value
     * @return bool
     */
    public function isValid($value);

    /**
     * @return string
     */
    public function getErrorMessage();
}
