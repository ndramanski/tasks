<?php

namespace Battleships\Validator;

use Config\Elements;

class StateInputValidator implements ValidatorInterface
{


    private $errorMsg;

    public function isValid($input)
    {
        if (!is_string($input)) {
            $this->errorMsg = Elements::INVALID_INPUT_GIVEN;
            return false;
        }
        if (strlen($input) < 2 || strlen($input) > 3) {
            $this->errorMsg = Elements::INVALID_INPUT_GIVEN;
            return false;
        }
        if (ctype_digit(substr($input, 0, 1))) {
            $this->errorMsg = Elements::INVALID_INPUT_GIVEN;
            return false;
        }
        if (!ctype_digit(substr($input, 1, 2))) {
            $this->errorMsg = Elements::INVALID_INPUT_GIVEN;
            return false;
        }
        if (substr($input, 1, 2)>Elements::COLS){
            $this->errorMsg = Elements::INVALID_INPUT_GIVEN;
            return false;
        }
        if (ord(substr($input, 0, 1))-32>(64 + Elements::ROWS)){
            $this->errorMsg = Elements::INVALID_INPUT_GIVEN;
            return false;
        }
        return true;
    }

    /**
     * @return string|null
     */
    public function getErrorMessage()
    {
        return $this->errorMsg;
    }
}
