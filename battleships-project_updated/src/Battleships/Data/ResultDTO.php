<?php

namespace Battleships\Data;

use Battleships\Validator\DTOValidator;

class ResultDTO
{
    private const USERNAME_MIN_LENGTH = 3;
    private const USERNAME_MAX_LENGTH = 25;

    private $id;
    private $username;
    private $result;
    private $createdOn;

    public function getResult()
    {
        return $this->result;
    }

    public function setResult($result): ResultDTO
    {
        $this->result = $result;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): ResultDTO
    {
        $this->id = $id;
        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param $username
     * @return ResultDTO
     * @throws \Exception
     */
    public function setUsername($username): ResultDTO
    {
        DTOValidator::validate(self::USERNAME_MIN_LENGTH, self::USERNAME_MAX_LENGTH,
            $username, "text", "Username");
        $this->username = $username;
        return $this;
    }

    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    public function setCreatedOn($createdOn): ResultDTO
    {
        $this->createdOn = $createdOn;
        return $this;
    }

}
