<?php

namespace Battleships\Service;


use Battleships\Data\ResultDTO;
use Battleships\Repository\ResultRepositoryInterface;

class ResultService
{
    /**
     * @var ResultRepositoryInterface
     */
    private $resultRepository;

    public function __construct(ResultRepositoryInterface $resultRepository)
    {
        $this->resultRepository = $resultRepository;
    }

    public function add(ResultDTO $resultDTO): bool
    {
        return $this->resultRepository->insert($resultDTO);
    }

    /**
     * @return \Generator|ResultDTO[]
     */
    public function getAll(): \Generator
    {
        return $this->resultRepository->findAll();
    }

}
