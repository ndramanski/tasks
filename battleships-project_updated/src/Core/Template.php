<?php

namespace Core;

use Config\Elements;

class Template implements TemplateInterface
{
    public function render(string $templateName, $data = null, array $errors = []): void
    {
        require_once Elements::TEMPLATE_FOLDER
            . $templateName
            . Elements::TEMPLATE_EXTENSION;
    }
}
