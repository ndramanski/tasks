<?php
namespace Config;

class Elements
{
    const SHIP_EXCEPTION = "Failed placing ship after %d retries";
    const INVALID_INPUT_GIVEN = "The given input is not a valid option.";

    const DIRECTION_VERTICAL = 'vertical';
    const DIRECTION_HORIZONTAL = 'horizontal';

    const TEMPLATE_FOLDER = "App/Template/";
    const TEMPLATE_EXTENSION = ".php";

    const HANDLE_RESULT_HIT = "*** Hit ***";
    const HANDLE_RESULT_MISS = "*** Miss ***";
    const HANDLE_RESULT_ERROR = "*** Error ***";
    const HANDLE_RESULT_SUNK = "*** Sunk ***";
    const HANDLE_GAME_OVER = "*** Game Over ***";

    const BOARD_SYMBOL_FULL = 'X';
    const BOARD_SYMBOL_EMPTY = ' ';
    const BOARD_SYMBOL_MISS = '-';
    const BOARD_SYMBOL_REGULAR = '.';

    const CHR_START_NUMBER = 64;

    const ROWS = 10;
    const COLS = 10;
    const GAME_NAME = "battleships";
    const SHIPS = [
        ["Battleship", 5],
        ["Destroyer", 4],
        ["Destroyer", 4],
    ];
    const TRY_PLACE_SHIPS = 100;

}
