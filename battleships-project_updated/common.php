<?php

error_reporting(E_ALL);
ini_set("display_errors", "on");

session_start();

require __DIR__ . '/vendor/autoload.php';

use Battleships\Controller\CliController;
use Battleships\Controller\WebController;
use Battleships\Helper\GameProcessor;
use Battleships\Helper\ShipPlacement\RandomShipPlacement;
use Battleships\Renderer\CliRenderer;
use Battleships\Helper\StateCreator;
use Battleships\Model\Ship;
use Battleships\Renderer\WebRenderer;
use Battleships\StateStorage\SessionStateStorage;
use Battleships\Validator\StateInputValidator;
use Config\Elements;
use Core\DataBinder;
use Core\Template;
use Battleships\Data\ResultDTO;
use Database\PDODatabase;
use Battleships\Service\ResultService;
use Battleships\Repository\ResultRepository;
use Database\DatabaseInterface;
use Battleships\Repository\DatabaseAbstract;


$config = [
    "verticalSize" => Elements::COLS,
    "horizontalSize" => Elements::ROWS,
    "gameName" => Elements::GAME_NAME,
    "ships" => Elements::SHIPS,
    "numberOfPlacementRetries" => Elements::TRY_PLACE_SHIPS,
];
$ships = [];
foreach ($config['ships'] as $shipProperties) {
    $ships[] = new Ship($shipProperties[0], $shipProperties[1]);
}
$config["shipPlacementService"] = new RandomShipPlacement($ships, $config['numberOfPlacementRetries']);
$config["stateCreator"] = new StateCreator($config['verticalSize'], $config['horizontalSize'],
    $config["shipPlacementService"]);
$config["stateStorage"] = new SessionStateStorage($config['gameName']);
$config["validator"] = new StateInputValidator();
$config["gameProcessor"] = new GameProcessor($config["validator"]);
$config["webRenderer"] = new WebRenderer();
$config["cliRenderer"] = new CliRenderer();
$template = new Template();
$dataBinder = new DataBinder();
$render = new WebRenderer();
$dbInfo = parse_ini_file("src/Config/db.ini");
$pdo = new PDO($dbInfo['dsn'], $dbInfo['user'], $dbInfo['pass']);
$db = new PDODatabase($pdo);
$resultRepository = new ResultRepository($db, $dataBinder);
$resultService = new ResultService($resultRepository);
