<?php

use Battleships\Data\ResultDTO;

require_once 'common.php';

$formData = $_POST;
if (isset($formData)) {
    $resultInfo = $dataBinder->bind($formData, ResultDTO::class);
    $resultInfo->setResult($formData['result']);
    try {
        $resultInfo->setUsername($formData['username']);
    } catch (Exception $e) {
        echo $e;
    }
    $date = new DateTime('now');
    $resultInfo->setCreatedOn($date->format('Y-m-d H:i:s'));
    $resultService->add($resultInfo);
    $render->viewStatistics($resultService->getAll());
}
