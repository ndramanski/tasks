<?php

require __DIR__ . '/vendor/autoload.php';

require_once 'common.php';

unset($_SESSION['battleships']);
session_destroy();
header("Location: index.php");
