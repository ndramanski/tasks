<?php

use LendingTestReworked\Constants;
use LendingTestReworked\Investor;
use LendingTestReworked\Tranche;
use PHPUnit\Framework\TestCase;

class InvestorTest extends TestCase
{
    private $investor;
    private $tranche;

    public function setUp(): void
    {
        $this->investor = new Investor(1000, 'TestInvestor');
    }

    public function testInvestorGetName()
    {
        $result = $this->investor->getName();
        $this->assertEquals('TestInvestor', $result);
    }

    public function testCanInvest()
    {
        $this->tranche = $this->createMock(Tranche::class);
        $this->tranche->expects($this->any())
            ->method('investMoney')
            ->will($this->returnSelf());
        $this->investor->invest(
            $this->tranche,
            500,
            DateTime::createFromFormat(Constants::DATE_FORMAT, '05/10/2015')
        );
        $this->assertEquals(
            0.00,
            $this
                ->investor
                ->calculateInterestForDate(DateTime::createFromFormat(Constants::DATE_FORMAT, '06/10/2015'))
        );
    }
}
