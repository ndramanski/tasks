<?php

use LendingTestReworked\Constants;
use LendingTestReworked\DateModifier;
use PHPUnit\Framework\TestCase;

class DateModifierTest extends TestCase
{
    private $date;

    public function setUp(): void
    {
        $this->date = new DateModifier();
    }

    public function testCreateLastDateOfMonth()
    {
        $result = $this->date->createLastDateOfMonth('01/10/2015');
        $this->assertEquals(DateTime::createFromFormat(Constants::DATE_FORMAT, '31/10/2015'), $result);
    }
}
