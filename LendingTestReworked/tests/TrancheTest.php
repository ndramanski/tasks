<?php

use LendingTestReworked\Constants;
use LendingTestReworked\Loan;
use LendingTestReworked\Tranche;
use PHPUnit\Framework\TestCase;

class TrancheTest extends TestCase
{
    private $tranche;
    private $loan;

    public function setUp(): void
    {
        $this->loan = new Loan(DateTime::createFromFormat(Constants::DATE_FORMAT, '01/10/2015'),
            DateTime::createFromFormat(Constants::DATE_FORMAT, '15/11/2015'));
        $this->tranche = new Tranche($this->loan, 'TestTranche', 1000, 3);
    }

    public function testTrancheGetName()
    {
        $result = $this->tranche->getName();
        $this->assertEquals('TestTranche', $result);
    }

    public function testTrancheGetRate()
    {
        $result = $this->tranche->getRate();
        $this->assertEquals('3', $result);
    }
}
