<?php

use LendingTestReworked\Constants;
use LendingTestReworked\Loan;
use PHPUnit\Framework\TestCase;

class LoanTest extends TestCase
{
    private $loan;

    public function setUp(): void
    {
        $this->loan = new Loan(DateTime::createFromFormat(Constants::DATE_FORMAT, '01/10/2015'),
            DateTime::createFromFormat(Constants::DATE_FORMAT, '15/11/2015'));
    }

    public function testLoanIsOpenForInvestment()
    {
        $actual = $this->loan->isOpenForInvestment(DateTime::createFromFormat(Constants::DATE_FORMAT,
            '15/10/2015'));
        $this->assertEquals(false, $actual);
    }

    public function testLoanIsNotOpenForInvestment()
    {
        $actual = $this->loan->isOpenForInvestment(DateTime::createFromFormat(Constants::DATE_FORMAT,
            '01/12/2015'));
        $this->assertEquals(false, $actual);
    }
}

