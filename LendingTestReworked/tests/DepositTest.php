<?php

use LendingTestReworked\Constants;
use LendingTestReworked\Deposit;
use LendingTestReworked\Tranche;
use PHPUnit\Framework\TestCase;

class DepositTest extends TestCase
{
    private $deposit;
    private $tranche;

    public function setUp(): void
    {
        $this->tranche = $this->getMockBuilder(Tranche::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->deposit = new Deposit(
            $this->tranche,
            500,
            DateTime::createFromFormat(Constants::DATE_FORMAT, '05/10/2015')
        );
    }

    public function testgGetAmount()
    {
        $result = $this->deposit->getAmount();
        $this->assertEquals(500, $result);
    }

    public function testgGetTranche()
    {
        $result = $this->deposit->getTranche();
        $this->assertEquals($this->tranche, $result);
    }
}
