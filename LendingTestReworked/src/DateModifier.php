<?php

namespace LendingTestReworked;

use DateTime;

class DateModifier
{
    /**
     * @param string $date
     * @return DateTime
     */
    public function modifyDate(string $date): \DateTime
    {
        return DateTime::createFromFormat(Constants::DATE_FORMAT, $date);
    }

    /**
     * @param string $startDate
     * @return string
     */
    public function daysOfMonth(string $startDate): string
    {
        $ymd = explode("/", $startDate);
        return strval(cal_days_in_month(CAL_GREGORIAN, $ymd[1], $ymd[2]));
    }

    /**
     * @param DateTime $investmentDate
     * @return int
     */
    public function interestDays(DateTime $investmentDate): int
    {
        return (int)$this->createLastDateOfMonth(Constants::START_DATE)->
            diff($investmentDate)->format("%a") + 1;
    }

    /**
     * @param string $startDate
     * @return DateTime
     */
    public function createLastDateOfMonth(string $startDate): \DateTime
    {
        $ymd = explode("/", $startDate);
        return $this->modifyDate($this->daysOfMonth($startDate) . "/" . $ymd[1] . "/" . $ymd[2]);
    }
}
