<?php

namespace LendingTestReworked;

use DateTime;
use Exception;

interface CalculationInterface
{
    /**
     * @param DateTime
     * @return float
     * @throws Exception
     */
    public function interestCalculation(DateTime $investmentDate): float;

}