<?php

namespace LendingTestReworked;

use DateTime;

interface InvestmentInterface
{
    /**
     * @param int $money
     * @param DateTime $depositDate
     */
    public function investMoney(int $money, DateTime $depositDate): void;
}
