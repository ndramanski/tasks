<?php

namespace LendingTestReworked;

use DateTime;

interface LoanInterface
{
    /**
     * @param DateTime $dateOfInvestment
     * @return bool
     */
    public function isOpenForInvestment(DateTime $dateOfInvestment): bool;

}