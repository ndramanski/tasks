<?php

namespace LendingTestReworked;

use DateTime;
use Exception;
use LendingTestReworked\Deposit;
use LendingTestReworked\DateModifier;

class Investor implements CalculationInterface
{
    /**
     * @var float
     */
    private $funds;
    /**
     * @var string
     */
    private $name;
    /**
     * @var Deposit
     */
    private $deposits;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Investor constructor.
     * @param float $funds
     * @param string $name
     * @throws Exception
     */

    public function __construct(float $funds, string $name)
    {
        $this->funds = $funds;
        $this->name = $name;
    }

    /**
     * @param Tranche $tranche
     * @param int $money
     * @param DateTime $date
     * @throws Exception
     */
    public function invest(Tranche $tranche, int $money, DateTime $date): void
    {
        $this->deposits = new Deposit($tranche, $money, $date);
        $this->updateFunds($money);
        $tranche->investMoney($money, $date);
    }

    /**
     * @param int $amount
     * @throws Exception
     */
    private function updateFunds(int $amount): void
    {
        if ($this->funds < $amount) {
            throw  new Exception('Not enough funds for this investment.');
        }
        $this->funds -= $amount;
    }

    /**
     * @param DateTime $investmentDate
     * @return float
     * @throws Exception
     */
    public function calculateInterestForDate(DateTime $investmentDate): float
    {
        $earnings = 0.00;
        $earnings += $this->interestCalculation($investmentDate);
        return $earnings;
    }

    public function interestCalculation(DateTime $investmentDate): float
    {
        $earned = ((($this->deposits->getTranche()->getRate() / 100) * $this->deposits->getAmount()) /
                (int)$investmentDate->format('t')) * $this->deposits->interestDays($investmentDate);
        return number_format($earned, 2);
    }
}
