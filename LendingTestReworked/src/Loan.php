<?php

namespace LendingTestReworked;

use DateTime;

class Loan implements LoanInterface
{
    /**
     * @var DateTime
     */
    private $endDate;
    /**
     * @var DateTime
     */
    private $startDate;

    /**
     * Loan constructor.
     * @param DateTime $endDate
     * @param DateTime $startDate
     */
    public function __construct(DateTime $endDate, DateTime $startDate)
    {
        $this->endDate = $endDate;
        $this->startDate = $startDate;
    }

    /**
     * @param DateTime $dateOfInvestment
     * @return bool
     */
    public function isOpenForInvestment(DateTime $dateOfInvestment): bool
    {
        return ($dateOfInvestment > $this->startDate && $dateOfInvestment < $this->endDate);
    }
}
