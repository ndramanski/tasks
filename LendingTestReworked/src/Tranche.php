<?php

namespace LendingTestReworked;

use DateTime;
use Exception;

class Tranche extends DateModifier implements InvestmentInterface
{
    /**
     * @var Loan
     */
    private $loan;
    /**
     * @var string
     */
    private $name;
    /**
     * @var float
     */
    private $trancheAmount;
    /**
     * @var int
     */
    private $rate;
    /**
     * @var int
     */
    private $deposit = 0;

    /**
     * Tranche constructor.
     * @param Loan $loan
     * @param string $name
     * @param float $trancheAmount
     * @param int $rate
     */
    public function __construct(Loan $loan, string $name, float $trancheAmount, int $rate)
    {
        $this->loan = $loan;
        $this->name = $name;
        $this->trancheAmount = $trancheAmount;
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getRate(): int
    {
        return $this->rate;
    }

    /**
     * @param int $moneyToInvest
     * @param DateTime $depositDate
     * @throws Exception
     */
    public function investMoney(int $moneyToInvest, DateTime $depositDate): void
    {
        if (!is_int($moneyToInvest)) {
            throw new Exception("$moneyToInvest - Value is not correct!");
        }
        if (!$this->loan->isOpenForInvestment($depositDate)) {
            throw new Exception("Not available for more investments");
        }
        if (($this->deposit + $moneyToInvest) > $this->trancheAmount) {
            throw new Exception("The investment is more, then the tranche amount!");
        }
        $this->deposit += $moneyToInvest;
    }
}
