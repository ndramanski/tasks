<?php

namespace LendingTestReworked;

use DateTime;
use Exception;

class Deposit extends DateModifier
{
    /**
     * @var Tranche
     */
    private $tranche;
    /**
     * @var int
     */
    private $amount;
    /**
     * @var DateTime
     */
    private $depositDate;

    /**
     * Deposit constructor.
     * @param Tranche $tranche
     * @param int $amount
     * @param DateTime $depositDate
     * @throws Exception
     */
    public function __construct(Tranche $tranche, int $amount, DateTime $depositDate)
    {
        $this->tranche = $tranche;
        $this->amount = $amount;
        $this->depositDate = $depositDate;
    }

    /**
     * @return Tranche
     */
    public function getTranche(): Tranche
    {
        return $this->tranche;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }
}
