<?php

namespace LendingTestReworked;

interface Constants
{
    const RATE = [3, 6];
    const TRANCHE_NAME = ['Trance A', 'Trance B'];
    const TRANCHE_AMOUNT = 1000;
    const AMOUNT_TO_INVEST = [1000, 1, 500, 1100];
    const INVESTOR_NAME = ["Investor 1", "Investor 2", "Investor 3", "Investor 4"];
    const INVESTOR_MONEY = 1000;
    const DATE_FORMAT = 'd/m/Y';
    const START_DATE = '01/10/2015';
    const END_DATE = '15/11/2015';
    const INTEREST_CALCULATION_DAY = '01/11/2015';
    const DEPOSIT_DATE = ['03/10/2015', '04/10/2015', '10/10/2015', '25/10/2015'];
}
