<?php

use LendingTestReworked\Loan;
use LendingTestReworked\DateModifier;
use LendingTestReworked\Investor;
use LendingTestReworked\Tranche;
use LendingTestReworked\Constants;

require __DIR__ . '/vendor/autoload.php';

$dateModifier = new DateModifier();

try {
    $loan = new Loan($dateModifier->modifyDate(Constants::END_DATE),
        $dateModifier->modifyDate(Constants::START_DATE));
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

try {
    $trancheA = new Tranche($loan, Constants::TRANCHE_NAME[0],
        Constants::TRANCHE_AMOUNT, Constants:: RATE[0]);
    $trancheB = new Tranche($loan, Constants::TRANCHE_NAME[1],
        Constants::TRANCHE_AMOUNT, Constants:: RATE[1]);
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

try {
    $investor1 = new Investor(Constants::INVESTOR_MONEY, Constants::INVESTOR_NAME[0]);
    $investor2 = new Investor(Constants::INVESTOR_MONEY, Constants::INVESTOR_NAME[1]);
    $investor3 = new Investor(Constants::INVESTOR_MONEY, Constants::INVESTOR_NAME[2]);
    $investor4 = new Investor(Constants::INVESTOR_MONEY, Constants::INVESTOR_NAME[3]);
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

try {
    $investor1->invest($trancheA, Constants::AMOUNT_TO_INVEST[0], $dateModifier->modifyDate(Constants::DEPOSIT_DATE[0]));
    echo $investor1->getName() . ' earns '
        . $investor1->calculateInterestForDate($dateModifier->modifyDate(Constants::DEPOSIT_DATE[0]))
        . ' pounds' . PHP_EOL;
} catch (Exception $e) {
    echo $investor1->getName() . ' ' . $e->getMessage() . PHP_EOL;
}

try {
    $investor2->invest($trancheA, Constants::AMOUNT_TO_INVEST[1], $dateModifier->modifyDate(Constants::DEPOSIT_DATE[1]));
    echo $investor2->getName() . ' earns '
        . $investor2->calculateInterestForDate($dateModifier->modifyDate(Constants::DEPOSIT_DATE[1]))
        . ' pounds' . PHP_EOL;
} catch (Exception $e) {
    echo $investor2->getName() . ' ' . $e->getMessage() . PHP_EOL;
}

try {
    $investor3->invest($trancheB, Constants::AMOUNT_TO_INVEST[2], $dateModifier->modifyDate(Constants::DEPOSIT_DATE[2]));
    echo $investor3->getName() . ' earns '
        . $investor3->calculateInterestForDate($dateModifier->modifyDate(Constants::DEPOSIT_DATE[2]))
        . ' pounds' . PHP_EOL;
} catch (Exception $e) {
    echo $investor3->getName() . ' ' . $e->getMessage() . PHP_EOL;
}

try {
    $investor4->invest($trancheB, Constants::AMOUNT_TO_INVEST[3], $dateModifier->modifyDate(Constants::DEPOSIT_DATE[3]));
    echo $investor4->getName() . ' earns '
        . $investor4->calculateInterestForDate($dateModifier->modifyDate(Constants::DEPOSIT_DATE[3]))
        . ' pounds' . PHP_EOL;
} catch (Exception $e) {
    echo $investor4->getName() . ' ' . $e->getMessage() . PHP_EOL;
}
