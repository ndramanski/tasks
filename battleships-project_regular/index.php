<?php

use Battleships\Controller\CliController;
use Battleships\Controller\WebController;

require __DIR__ . '/vendor/autoload.php';
require_once 'common.php';

if (php_sapi_name() === 'cli') {
    $controller = new CliController($config["stateStorage"],
        $config["stateCreator"], $config["gameProcessor"], $config["cliRenderer"]);
} else {
    $controller = new WebController($config["stateStorage"],
        $config["stateCreator"], $config["gameProcessor"], $config['webRenderer']);
}
$controller->run();
