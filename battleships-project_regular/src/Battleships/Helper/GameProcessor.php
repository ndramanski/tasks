<?php

namespace Battleships\Helper;

use Battleships\Model\BoardField;
use Battleships\Model\Ship;
use Battleships\Model\State;
use Battleships\Validator\ValidatorInterface;
use Config\Elements;

class GameProcessor
{
    private $inputValidator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->inputValidator = $validator;
    }

    /**
     * @param State $state
     * @param string $input
     * @return string
     */
    public function handleInput(State $state, string $input): string
    {
        if (!$this->inputValidator->isValid($input)) {
            return Elements::HANDLE_RESULT_ERROR;
        }
        $boardField = $state->getBoard()->getBoardFieldByName($input);
        $state->setShotsTaken($state->getShotsTaken() + 1);
        if ($boardField->isHit()) {
            return Elements::HANDLE_RESULT_MISS;
        }
        $boardField->setIsHit(true);
        if (!$boardField->hasShip()) {
            return Elements::HANDLE_RESULT_MISS;
        }
        if ($this->checkIfGameIsOver($state)) {
            return Elements::HANDLE_GAME_OVER;
        }
        if ($this->checkIfShipHasSunk($state, $boardField->getShip())) {
            return Elements::HANDLE_RESULT_SUNK;
        }
        return Elements::HANDLE_RESULT_HIT;
    }

    /**
     * @param State $state
     * @return bool
     */
    private function checkIfGameIsOver(State $state): bool
    {
        $gameOver = true;
        foreach ($state->getBoard()->getGrid() as $column => $row) {
            /** @var BoardField $boardField */
            foreach ($row as $boardField) {
                if ($boardField->hasShip() && !$boardField->isHit()) {
                    return false;
                }
            }
        }
        return $gameOver;
    }

    /**
     * @param State $state
     * @param Ship $ship
     * @return bool
     */
    private function checkIfShipHasSunk(State $state, Ship $ship): bool
    {
        $shipHasSunk = true;

        foreach ($state->getBoard()->getGrid() as $column => $row) {
            /* @var $row BoardField[] */
            foreach ($row as $boardField) {
                if ($boardField->hasShip() && $boardField->getShip() === $ship && !$boardField->isHit()) {
                    $shipHasSunk = false;
                }
            }
        }
        return $shipHasSunk;
    }

}
