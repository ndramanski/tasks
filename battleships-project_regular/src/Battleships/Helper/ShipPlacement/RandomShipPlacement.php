<?php

namespace Battleships\Helper\ShipPlacement;

use Battleships\Model\Board;
use Battleships\Model\Ship;
use Battleships\Exception\CannotPlaceShipException;
use Config\Elements;

class RandomShipPlacement implements ShipPlacementInterface
{
    /**
     * @var Ship[]
     */
    private $ships = [];
    /**
     * @var int
     */
    private $maxNumberOfRetries;

    /**
     * @param array $ships
     * @param int $numberOfRetries
     */
    public function __construct(array $ships, int $numberOfRetries)
    {
        $this->ships = $ships;
        $this->maxNumberOfRetries = $numberOfRetries;
    }

    /**
     * @param Board $board
     * @return mixed
     * @throws CannotPlaceShipException
     */
    public function placeShipsOnBoard(Board $board)
    {
        foreach ($this->ships as $ship) {
            $placed = false;
            for ($retryNumber = 0; $retryNumber < $this->maxNumberOfRetries; ++$retryNumber) {
                $direction = rand(1, 2) === 1 ? Elements::DIRECTION_VERTICAL : Elements::DIRECTION_HORIZONTAL;
                if ($direction === Elements::DIRECTION_VERTICAL) {
                    if ($this->placeVerticalShipOnBoard($board, $ship)) {
                        $placed = true;
                        break;
                    }
                } else if ($direction === Elements::DIRECTION_HORIZONTAL) {
                    if ($this->placeHorizontalShipOnBoard($board, $ship)) {
                        $placed = true;
                        break;
                    }
                }
            }
            if (!$placed) {
                throw new CannotPlaceShipException(sprintf(Elements::SHIP_EXCEPTION, $this->maxNumberOfRetries));
            }
        }
        return true;
    }

    /**
     * @param Board $board
     * @param Ship $ship
     * @return bool
     */
    public function placeVerticalShipOnBoard(Board $board, Ship $ship): bool
    {
        $columnNumber = rand(1, $board->getBoardHeight());
        $rowNumber = rand(1, $board->getBoardWidth());
        if ($columnNumber + $ship->getSize() > $board->getBoardHeight() + 1) {
            return false;
        }
        for ($i = $columnNumber; isset($board->getGrid()[$i]); $i++) {
            $boardField = $board->getBoardFieldByCoordinates($i, $rowNumber);
            if ($boardField->hasShip()) {
                return false;
            }
        }
        for ($i = $columnNumber; $i < $columnNumber + $ship->getSize(); $i++) {
            $board->getBoardFieldByCoordinates($i, $rowNumber)->setShip($ship);
        }
        return true;
    }

    /**
     * @param Board $board
     * @param Ship $ship
     * @return bool
     */
    public function placeHorizontalShipOnBoard(Board $board, Ship $ship): bool
    {
        $columnNumber = rand(1, $board->getBoardHeight());
        $rowNumber = rand(1, $board->getBoardWidth());
        if ($rowNumber + $ship->getSize() > $board->getBoardWidth() + 1) {
            return false;
        }
        for ($i = $rowNumber; isset($board->getGrid()[$columnNumber][$i]); $i++) {
            $boardField = $board->getBoardFieldByCoordinates($columnNumber, $i);
            if ($boardField->hasShip()) {
                return false;
            }
        }
        for ($i = $rowNumber; $i < $rowNumber + $ship->getSize(); $i++) {
            $board->getBoardFieldByCoordinates($columnNumber, $i)->setShip($ship);
        }
        return true;
    }

}
