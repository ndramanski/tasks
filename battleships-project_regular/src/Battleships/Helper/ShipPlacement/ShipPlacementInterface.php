<?php

namespace Battleships\Helper\ShipPlacement;

use Battleships\Exception\CannotPlaceShipException;
use Battleships\Model\Board;
use Battleships\Model\Ship;

interface ShipPlacementInterface
{
    /**
     * @param Board $board
     * @return mixed
     * @throws CannotPlaceShipException
     */
    public function placeShipsOnBoard(Board $board);
}
