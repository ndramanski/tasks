<?php

namespace Battleships\Renderer;

use Battleships\Model\BoardField;
use Config\Elements;

abstract class AbstractRenderer
{
    protected $emptySpace = Elements::BOARD_SYMBOL_EMPTY;
    protected $newLine = PHP_EOL;
    protected $fieldIsHitSymbol = Elements::BOARD_SYMBOL_MISS;
    protected $shipIsHitSymbol = Elements::BOARD_SYMBOL_FULL;
    protected $noHitSymbol = Elements::BOARD_SYMBOL_REGULAR;

    /**
     * @param BoardField $boardField
     * @param bool $activateCheat
     */
    protected function renderBoardField(BoardField $boardField, bool $activateCheat)
    {
        if (($activateCheat || $boardField->isHit()) && $boardField->hasShip()) {
            echo ($activateCheat && $boardField->isHit()) ? $this->emptySpace : $this->shipIsHitSymbol;
        } else if ($boardField->isHit()) {
            echo ($activateCheat && $boardField->isHit()) ? $this->emptySpace : $this->fieldIsHitSymbol;
        } else {
            echo $activateCheat ? $this->emptySpace : $this->noHitSymbol;
        }
        echo str_repeat($this->emptySpace, 2);
    }
}
